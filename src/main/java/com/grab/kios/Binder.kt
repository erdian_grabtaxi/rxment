package com.grab.kios

import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.processors.BehaviorProcessor
import io.reactivex.rxjava3.processors.FlowableProcessor
import io.reactivex.rxjava3.processors.PublishProcessor

class Binder : android.os.Binder() {
    private val countProcessor : FlowableProcessor<Int> = BehaviorProcessor.create()
    private val stateProcessor : FlowableProcessor<Boolean> = BehaviorProcessor.createDefault(false)
    private val commandProcessor : FlowableProcessor<Int> = PublishProcessor.create()

    fun getState() : Flowable<Boolean> {
        return stateProcessor
    }

    fun getCounter() : Flowable<Int> {
        return countProcessor
    }

    fun start(count : Int) {
        commandProcessor.onNext(count)
    }

    fun getCommand() : Flowable<Int> {
        return commandProcessor
    }

    fun setCount(count: Int) {
        countProcessor.onNext(count)
    }

    fun setState(state : Boolean) {
        stateProcessor.onNext(state)
    }
}