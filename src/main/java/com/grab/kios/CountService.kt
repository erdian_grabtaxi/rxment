package com.grab.kios

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import io.reactivex.rxjava3.core.Flowable
import java.util.concurrent.TimeUnit

class CountService : Service() {
    private val binder : Binder = Binder()

    override fun onCreate() {
        super.onCreate()

        Log.e("Service", "Service Created")
        initEventListener()
    }

    private fun initEventListener() {
        binder.getCommand()
            .concatMap {
                Flowable.intervalRange(0, it.toLong() + 1, 0, 1, TimeUnit.SECONDS)
                    .doOnSubscribe { binder.setState(true) }
                    .doOnComplete { binder.setState(false) }
            }.subscribe { binder.setCount(it.toInt()) }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }
}