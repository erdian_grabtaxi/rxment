package com.grab.kios

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.TextView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.processors.FlowableProcessor
import io.reactivex.rxjava3.processors.PublishProcessor
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

class MainActivity : ServiceConnection, AppCompatActivity() {
    private val minCount: Int = 0
    private val maxCount: Int = 9
    private val keyCounterState = "KEY_COUNTER_STATE"
    private val counter: AtomicInteger = AtomicInteger()
    private val counterFlow : FlowableProcessor<Boolean> = PublishProcessor.create()

    // ** method service **
    private lateinit var binder : Binder
    private val subscriber : CompositeDisposable = CompositeDisposable()
    private val numMap : HashMap<Int, String> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            counter.set(savedInstanceState.getInt(keyCounterState))
        }

        initNumMap()
        initEventListener()

        // ** method service **
        if (!this::binder.isInitialized) {
            val service = Intent(this, CountService::class.java)
            bindService(service, this, BIND_AUTO_CREATE)
        }
    }

    private fun initNumMap() {
        numMap[0] = getString(R.string.text_0)
        numMap[1] = getString(R.string.text_1)
        numMap[2] = getString(R.string.text_2)
        numMap[3] = getString(R.string.text_3)
        numMap[4] = getString(R.string.text_4)
        numMap[5] = getString(R.string.text_5)
        numMap[6] = getString(R.string.text_6)
        numMap[7] = getString(R.string.text_7)
        numMap[8] = getString(R.string.text_8)
        numMap[9] = getString(R.string.text_9)
    }

    private fun initEventListener() {
        counterFlow
            .map {
                var value = counter.get()
                if (it && maxCount > value) {
                    value = counter.incrementAndGet()
                } else if (!it && minCount < value){
                    value = counter.decrementAndGet()
                }
                value
            }
            .startWithItem(counter.get())
            .subscribe { findViewById<TextView>(R.id.lb_value).text = it.toString() }

        findViewById<View>(R.id.bt_minus).setOnClickListener { onButtonMinusClick() }
        findViewById<View>(R.id.bt_plus).setOnClickListener { onButtonPlusClick() }
        // ** method direct **
//        findViewById<View>(R.id.bt_start).setOnClickListener { onButtonStartClick() }
    }

    private fun onButtonMinusClick() {
        counterFlow.onNext(false)
    }

    private fun onButtonPlusClick() {
        counterFlow.onNext(true)
    }

    private fun onButtonStartClick() {
        // ** method direct **
//        Flowable.intervalRange(0, counter.get().toLong() + 1, 0, 1, TimeUnit.SECONDS)
//            .observeOn(AndroidSchedulers.mainThread())
//            .doOnSubscribe { findViewById<View>(R.id.bt_start).isEnabled = false }
//            .doOnComplete { findViewById<View>(R.id.bt_start).isEnabled = true }
//            .subscribe {
//                Log.e("GRAB", "Current Counter: " + it)
//                findViewById<TextView>(R.id.lb_count).text = numMap[it.toInt()]
//            }

        // ** method service **
        binder.start(counter.get())
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(keyCounterState, counter.get())
        super.onSaveInstanceState(outState)
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        binder = service as Binder

        subscriber.add(binder
            .getCounter()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { findViewById<TextView>(R.id.lb_count).text = numMap[it] })
        subscriber.add(binder
            .getState()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { findViewById<View>(R.id.bt_start).isEnabled = !it })
        findViewById<View>(R.id.bt_start).setOnClickListener { onButtonStartClick() }
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        subscriber.clear()
    }
}